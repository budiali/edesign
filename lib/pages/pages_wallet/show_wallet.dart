import 'package:edesign/models/models_wallet/m_wallet.dart';
import 'package:edesign/repositories/repo_wallet/wallet_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:edesign/helper/numberFormat.dart';

class ShowWallet extends StatefulWidget {
  // const ShowWallet({ Key? key }) : super(key: key);

  @override
  _ShowWalletState createState() => _ShowWalletState();
}

class _ShowWalletState extends State<ShowWallet> {
  Future<Wallet> _futureWallet;

  @override
  void initState() {
    _futureWallet = WalletRepository.getWallet();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Wallet'),
          centerTitle: false,
          elevation: 0,
          backgroundColor: Colors.blueAccent,
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back)),
        ),
        body: Center(
          child: buildPageWallet(),
        ),
      ),
    );
  }

  Widget buildPageWallet() {
    return FutureBuilder(
        future: _futureWallet,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.none &&
              snapshot.hasData == null) {
            print("Object not found");
          }
          if (snapshot.hasData) {
            return Container(
              // width: 100,
              // height: 100,
              // color: Colors.pink,
              child: Column(
                children: [
                  Flexible(
                    child: Container(
                      child: Row(
                        children: [
                          Flexible(
                            child: Container(
                              margin: EdgeInsets.only(left: 5, top: 50),
                              child: Text(
                                'Saldo Anda :',
                                style: TextStyle(fontSize: 30),
                              ),
                            ),
                          ),
                          Flexible(
                            child: Container(
                              margin: EdgeInsets.only(left: 5, top: 50),
                              child: Text(
                                formatNumber(snapshot.data.amount).toString(),
                                style: TextStyle(fontSize: 30),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Flexible(
                    child: Container(
                      child: Row(
                        children: [
                          Flexible(
                            child: Container(
                              margin: EdgeInsets.only(left: 5, top: 20),
                              child: Text(
                                'Dana Tertahan :',
                                style: TextStyle(fontSize: 30),
                              ),
                            ),
                          ),
                          Flexible(
                            child: Container(
                              margin: EdgeInsets.only(left: 5, top: 20),
                              child: Text(
                                formatNumber(snapshot.data.hold).toString(),
                                style: TextStyle(fontSize: 30),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            );
          }
          return CircularProgressIndicator();
        });
  }
}
