import 'dart:convert';
import 'package:edesign/models/models_signup/signup.dart';
import 'package:edesign/repositories/repo_signup/signup_repo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
// import 'package:email_validator/email_validator.dart';
// import 'package:dcdg/dcdg.dart';

import '../main.dart';
import 'HomePage.dart';

final storage = new FlutterSecureStorage();

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Colors.red[400], Colors.deepPurple, Colors.blue])),
      child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Center(child: BuildFormLogin())),
    );
  }
}

class BuildFormLogin extends StatelessWidget {
  // const ({ Key? key }) : super(key: key);
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  void displayDialog(context, title, text) => showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(title: Text(title), content: Text(text)),
      );

  Future<String> attemptLogIn(String username, String password) async {
    var res = await http.post(Uri.parse("$SERVER_IP/auth/token/"),
        body: {"username": username, "password": password});
    if (res.statusCode == 200) return res.body;
    print(res.body);
    return null;
  }

  // Future<String> attemptSignUp(String username, String password) async {
  //   var res = await http.post(Uri.parse("$SERVER_IP/auth/token/"),
  //       body: {"username": username, "password": password});
  //   if (res.statusCode == 200) return res.body;
  //   print(res.body);
  //   return null;
  // }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 350,
      height: 330,
      child: Card(
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              width: 250.0,
              height: 30.0,
              margin: EdgeInsets.only(top: 15.0),
              // color: Colors.amber,
              child: Text(
                'Login eDesign application for customer',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15.0),
              width: 300.0,
              child: TextField(
                controller: _usernameController,
                decoration: InputDecoration(labelText: 'Username'),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15.0),
              width: 300.0,
              child: TextField(
                controller: _passwordController,
                obscureText: true,
                decoration: InputDecoration(labelText: 'Password'),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 30.0),
              width: 300.0,
              child: MaterialButton(
                minWidth: double.infinity,
                onPressed: () async {
                  var username = _usernameController.text;
                  var password = _passwordController.text;
                  var res = await attemptLogIn(username, password);
                  var jwt;
                  if (res != null) {
                    jwt = json.decode(res);
                  }

                  if (jwt != null) {
                    storage.write(key: 'access', value: jwt['access']);
                    storage.write(key: 'refresh', value: jwt['refresh']);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => HomePage.tokenObject(jwt)));
                  } else {
                    displayDialog(context, "Terjadi kesalahan",
                        "Username dan password tidak ditemukan.");
                  }
                },
                color: Colors.blueAccent,
                child: Container(
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(30)),
                  child: Text(
                    "Login",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
            Container(
              width: 250.0,
              // color: Colors.red,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 37),
                      width: 110,
                      // color: Colors.amber,
                      child: Text('Do you have account?'),
                    ),
                    Container(
                      width: 60,
                      height: 30,
                      // color: Colors.amber,
                      child: TextButton(
                          style: TextButton.styleFrom(
                              textStyle:
                                  TextStyle(color: Colors.redAccent[400])),
                          // onPressed: () async {
                          //   var username = _usernameController.text;
                          //   var password = _passwordController.text;

                          //   if (username.length < 4)
                          //     displayDialog(context, "Invalid Username",
                          //         "The username should be at least 4 characters long");
                          //   else if (password.length < 4)
                          //     displayDialog(context, "Invalid Password",
                          //         "The password should be at least 4 characters long");
                          //   else {
                          //     var res = await attemptSignUp(username, password);
                          //     if (res == 201)
                          //       displayDialog(context, "Success",
                          //           "The user was created. Log in now.");
                          //     else if (res == 409)
                          //       displayDialog(
                          //           context,
                          //           "That username is already registered",
                          //           "Please try to sign up using another username or log in if you already have an account.");
                          //     else {
                          //       displayDialog(context, "Error",
                          //           "An unknown error occurred.");
                          //     }
                          //   }
                          // },
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SignUpPage()));
                          },
                          child: Text(
                            'Sign Up.',
                            style: TextStyle(color: Colors.redAccent),
                          )),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SignUpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            body: Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Colors.red[400], Colors.deepPurple, Colors.blue])),
      child: Center(
        child: SignUpForm(),
      ),
    )));
  }
}

class SignUpForm extends StatefulWidget {
  // const SignUpForm({ Key? key }) : super(key: key);

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final TextEditingController _uNameController = TextEditingController();
  final TextEditingController _pwController = TextEditingController();
  final TextEditingController _repeatPwController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  Future<SignUp> _futureSignUp;

  String username;
  String password;
  String password2;
  String email;
  String first_name;
  String last_name;

  void displayDialog(context, title, text) => showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(title: Text(title), content: Text(text)),
      );

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Center(
        child: SingleChildScrollView(
          child: Flexible(
            fit: FlexFit.loose,
            child: Container(
              width: 350,
              height: 550,
              child: Card(
                child: Column(
                  children: [
                    Flexible(
                      fit: FlexFit.loose,
                      child: Container(
                        alignment: Alignment.center,
                        width: 250.0,
                        height: 30.0,
                        margin: EdgeInsets.only(top: 25.0),
                        // color: Colors.amber,
                        child: Text(
                          'Register for edesign customer application',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16.0),
                        ),
                      ),
                    ),
                    Flexible(
                      fit: FlexFit.loose,
                      child: Container(
                        // margin: EdgeInsets.only(top: 10.0),
                        width: 300.0,
                        child: TextFormField(
                          controller: _firstNameController,
                          decoration: InputDecoration(labelText: 'Firstname'),
                          // ignore: missing_return
                          validator: (_firstNameController) {
                            if (_firstNameController == null ||
                                _firstNameController.isEmpty) {
                              return "First Name tidak boleh kosong";
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                    Flexible(
                      fit: FlexFit.loose,
                      child: Container(
                        margin: EdgeInsets.only(top: 15.0),
                        width: 300.0,
                        child: TextFormField(
                          controller: _lastNameController,
                          decoration: InputDecoration(labelText: 'Lastname'),
                          validator: (_lastNameController) {
                            if (_lastNameController == null ||
                                _lastNameController.isEmpty) {
                              return "Last Name tidak boleh kosong";
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                    Flexible(
                      fit: FlexFit.loose,
                      child: Container(
                        margin: EdgeInsets.only(top: 15.0),
                        width: 300.0,
                        child: TextFormField(
                          controller: _emailController,
                          decoration: InputDecoration(labelText: 'Email'),
                          // ignore: missing_return
                          validator: (_emailController) {
                            if (_emailController == null ||
                                _emailController.isEmpty) {
                              return "Email tidak boleh kosong";
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                    Flexible(
                      fit: FlexFit.loose,
                      child: Container(
                        width: 300.0,
                        child: TextFormField(
                          controller: _uNameController,
                          decoration: InputDecoration(labelText: 'Username'),
                          // ignore: missing_return
                          validator: (_uNameController) {
                            if (_uNameController == null ||
                                _uNameController.isEmpty) {
                              return "Username tidak boleh kosong";
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                    Flexible(
                      fit: FlexFit.loose,
                      child: Container(
                        margin: EdgeInsets.only(top: 15.0),
                        width: 300.0,
                        child: TextFormField(
                          controller: _pwController,
                          obscureText: true,
                          decoration: InputDecoration(labelText: 'Password'),
                          // ignore: missing_return
                          validator: (_pwController) {
                            if (_pwController == null ||
                                _pwController.isEmpty) {
                              return "Password tidak boleh kosong";
                            } else if (_pwController.length <= 8) {
                              return "Password harus terdiri dari 8 karakter";
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                    Flexible(
                      fit: FlexFit.loose,
                      child: Container(
                        margin: EdgeInsets.only(top: 15.0),
                        width: 300.0,
                        child: TextFormField(
                          controller: _repeatPwController,
                          obscureText: true,
                          decoration:
                              InputDecoration(labelText: 'Repeat Password'),
                          validator: (_repeatPwController) {
                            if (_repeatPwController == null ||
                                _repeatPwController.isEmpty) {
                              return "Password Retype tidak boleh kosong";
                            } else if (_repeatPwController.length <= 8) {
                              return "Password harus terdiri dari 8 karakter";
                            } else if (_repeatPwController !=
                                _pwController.text) {
                              return "Password tidak sama";
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                    Flexible(
                      fit: FlexFit.loose,
                      child: Container(
                        margin: EdgeInsets.only(top: 30.0),
                        width: 300.0,
                        child: MaterialButton(
                          minWidth: double.infinity,
                          onPressed: () async {
                            var username = _uNameController.text;
                            var password = _pwController.text;
                            var password2 = _repeatPwController.text;
                            var email = _emailController.text;
                            var first_name = _firstNameController.text;
                            var last_name = _lastNameController.text;

                            // if (username.length < 4) {
                            //   displayDialog(context, "Invalid Username",
                            //       "Username must be contain 4 characters.");
                            // } else if (password.length < 8) {
                            //   displayDialog(context, "Invalid Password",
                            //       "Password must be contain 8 characters.");
                            // } else if(){

                            // }

                            setState(() {
                              if (_formKey.currentState.validate() != true) {
                                return null;
                              } else {
                                _futureSignUp = SignUpRepository.attemptSignUp(
                                    username,
                                    password,
                                    password2,
                                    email,
                                    first_name,
                                    last_name);
                              }
                            });
                          },
                          color: Colors.blueAccent,
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30)),
                            child: Text(
                              "SignUp",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
