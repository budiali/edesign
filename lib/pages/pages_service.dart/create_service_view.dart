import 'package:edesign/models/models_service/create_service.dart';
import 'package:edesign/repositories/repo_service/create_service_repository.dart';
import 'package:flutter/material.dart';

class CreateServiceView extends StatefulWidget {
  // const CreateServiceView({Key? key,}) : super(key: key);

  @override
  _CreateServiceViewState createState() => _CreateServiceViewState();
}

class _CreateServiceViewState extends State<CreateServiceView> {
  final TextEditingController _controllerCategory = TextEditingController();
  final TextEditingController _controllerName = TextEditingController();
  final TextEditingController _controllerPrice = TextEditingController();
  final TextEditingController _controllerDesc = TextEditingController();

  Future<Service> _futureDataService;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Create Service"),
        ),
        body: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(8.0),
          child: (_futureDataService == null) ? buildColumn() : null,
        ),
      ),
    );
  }

  Column buildColumn() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        TextField(
          controller: _controllerCategory,
          decoration: InputDecoration(hintText: 'Enter Category'),
        ),
        TextField(
          controller: _controllerName,
          decoration: InputDecoration(hintText: 'Enter Name'),
        ),
        TextField(
          controller: _controllerPrice,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(hintText: 'Enter Price'),
        ),
        TextField(
          controller: _controllerDesc,
          decoration: InputDecoration(hintText: 'Enter Description'),
        ),
        ElevatedButton(
          onPressed: () {
            setState(() {
              _futureDataService = CreateServiceRepository.createService(
                  int.parse(_controllerCategory.text),
                  _controllerName.text,
                  double.parse(_controllerPrice.text),
                  _controllerDesc.text);
            });
          },
          child: Text('Create Data'),
        ),
        ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text("Back"))
      ],
    );
  }

  void updateService() {}
}
