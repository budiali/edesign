import 'package:edesign/helper/numberFormat.dart';
import 'package:edesign/models/models_finishing/m_finishing.dart';
import 'package:edesign/models/models_printable/m_printable.dart';
import 'package:edesign/repositories/repo_finishing/finishing_repository.dart';
import 'package:edesign/repositories/repo_order/post_create_order.dart';
import 'package:edesign/repositories/repo_printable/printable_repository.dart';
import 'package:flutter/material.dart';
import 'package:edesign/models/models_service/read_service.dart';
import 'package:edesign/repositories/repo_service/read_service_repository.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../pages_order/detail_order_view.dart';

final storage = new FlutterSecureStorage();

class DetailServiceView extends StatefulWidget {
  // const DetailServiceView({Key? key , this.id }) : super(key: key);
  DetailServiceView({this.id});
  final String id;
  @override
  _DetailServiceViewState createState() => _DetailServiceViewState();
}

class _DetailServiceViewState extends State<DetailServiceView> {
  final TextEditingController _quantity = TextEditingController();
  final TextEditingController _printable = TextEditingController();
  final TextEditingController _finishing = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  Future<ReadService> _readService;
  Map<String, dynamic> _createOrder;
  Future<List<Printable>> _futurePrintable;
  Future<List<Finishing>> _futureFinishing;

  // List<Printable> x
  int _mySelection;
  int _mySelected;
  bool enableFinishing = false;
  bool enableQty = false;

  // Map<String, dynamic> _mySelection = {'id': '', 'title': 'Select Title'};

  @override
  void initState() {
    _readService = ReadServiceRepository.fetchReadService(widget.id);
    _futurePrintable = PrintableRepository.getPrintables();
    _futureFinishing = FinishingRepository.getFinishing();
    super.initState();
  }

  @override
  void dispose() {
    _quantity.dispose();
    super.dispose();
  }

  bool _validate = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          centerTitle: false,
          elevation: 0,
          title: Text("Detail Service"),
          backgroundColor: Colors.blueAccent,
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back)),
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                (_readService != null) ? buildPage() : Text("Data Kosong"),
                buildForm()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildPage() {
    return FutureBuilder(
        future: _readService,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
              alignment: Alignment.topCenter,
              child: SingleChildScrollView(
                child: Card(
                  semanticContainer: true,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      // mainAxisAlignment: MainAxisAlignment.center,
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ListTile(
                          leading: Icon(Icons.album),
                          title: Text(snapshot.data.name),
                          subtitle: Text(
                              formatNumber(snapshot.data.price).toString()),
                        ),
                        Container(
                          // crossAxisAlignment: CrossAxisAlignment.stretch,
                          child: Padding(
                            padding: EdgeInsets.only(
                                left: 24.0,
                                right: 24.0,
                                top: 1.0,
                                bottom: 30.0),
                            child: Text(snapshot.data.desc),
                          ),
                          // buildForm(),
                        ),
                        // buildOrderButton(),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
          // By default, show a loading spinner.
          return CircularProgressIndicator();
        });
  }

  // String _valOrderList;
  // Future<List<Order>> _dataOrder = List() as Future<List<Order>>;
  Widget buildForm() {
    return Form(
      key: _formKey,
      child: Container(
        alignment: Alignment.topCenter,
        child: Card(
          child: Flexible(
            fit: FlexFit.loose,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 15.0, bottom: 15.0),
                  width: 150.0,
                  height: 30,
                  alignment: Alignment.center,

                  // color: Colors.amber,
                  child: Text(
                    "Form Pesanan",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                  child: FutureBuilder<List<Printable>>(
                    future: _futurePrintable,
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData) {
                        List<Printable> x = snapshot.data;
                        // print(x);
                        return DropdownButton(
                          isExpanded: true,
                          value: _mySelection,
                          onChanged: (newValue) {
                            setState(() {
                              _mySelection = newValue;
                              enableFinishing = true;
                            });
                          },
                          items: x.map<DropdownMenuItem>((Printable value) {
                            return DropdownMenuItem(
                              value: value.id,
                              child: Text(
                                value.name,
                                overflow: TextOverflow.ellipsis,
                              ),
                            );
                          }).toList(),
                        );
                      } else if (snapshot.hasError) {
                        return Text("${snapshot.error}");
                      }
                      return CircularProgressIndicator();
                    },
                  ),
                ),
                _buildDropDown(enableFinishing),
                buildOrderButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildDropDown(bool enable) {
    if (enable) {
      return Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: FutureBuilder<List<Finishing>>(
              future: _futureFinishing,
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  List<Finishing> x = snapshot.data;
                  // print(x);
                  return DropdownButton(
                    isExpanded: true,
                    value: _mySelected,
                    onChanged: (valuex) {
                      setState(() {
                        _mySelected = valuex;
                      });
                    },
                    items: x.map<DropdownMenuItem>((Finishing val) {
                      return DropdownMenuItem(
                        value: val.id,
                        child: Text(val.name.toString()),
                      );
                    }).toList(),
                  );
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }
                return CircularProgressIndicator();
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: TextFormField(
              controller: _quantity,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Quantity 1-10',
                  errorText: _validate ? 'Pesanan minimal 1' : null),
              // ignore: missing_return
              validator: (_quantity) {
                if (_quantity == null || _quantity.isEmpty) {
                  return "* Kuantitas Print pesanan minimal 1";
                }
                return null;
              },
            ),
          ),
        ],
      );
    } else {
      return Divider(color: Colors.white, height: 0.0);
    }
  }

  Widget buildOrderButton() {
    return FutureBuilder(
        future: _readService,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Icon(Icons.add_task),
                  SizedBox(width: 5),
                  TextButton(
                    child: const Text('PESAN'),
                    onPressed: () {
                      setState(() {
                        if (_formKey.currentState.validate() != true) {
                          return null;
                        } else {
                          postOrder(context, int.tryParse(snapshot.data.id));
                        }
                      });
                    },
                  ),
                ],
              ),
            );
          }
          return CircularProgressIndicator();
        });
  }

  Widget buildQuantity() {
    return Container(
      margin: EdgeInsets.only(top: 20, left: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Quantity :',
            style: TextStyle(fontSize: 15),
          ),
          SizedBox(
            height: 7,
          ),
          Row(
            children: [
              Container(
                width: 35,
                height: 35,
                margin: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                      'assets/button_min.png',
                    ),
                  ),
                ),
              ),
              Text(
                '2',
                style: TextStyle(fontSize: 15),
              ),
              Container(
                width: 35,
                height: 35,
                margin: EdgeInsets.only(left: 16),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                      'assets/button_add.png',
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void postOrder(BuildContext context, int id) async {
    _createOrder = await PostCreateOrder.postCreateOrder(
        id, int.tryParse(_quantity.text), _mySelection, _mySelected);
    if (_createOrder["message"] != null) {
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: _createOrder["message"],
        ),
      );
      // Navigator.pop(context);
      // Navigator.push(
      //     context, MaterialPageRoute(builder: (context) => TampilCategory()));
    } else {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  DetaileOrderView(id: _createOrder['id'].toString())));
    }
  }
}
