import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:url_launcher/url_launcher.dart';

// ignore: must_be_immutable
class Dashboard extends StatelessWidget {
  get callbackFunction => null;

  // const Dashboard({ Key? key }) : super(key: key);
  String desc =
      "Versions of the Lorem ipsum text have been used in typesetting at least since the 1960s, when it was popularized by advertisements for Letraset transfer sheets.[1] Lorem ipsum was introduced to the digital world in the mid-1980s, when Aldus employed it in graphic and word-processing templates for its desktop publishing program PageMaker. ";

  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      children: [
        CarouselSlider(
            items: [
              Container(
                margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    image: DecorationImage(
                        image: AssetImage('assets/carousel/logo_mockup.jpg'),
                        fit: BoxFit.fitWidth)),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    image: DecorationImage(
                        image: AssetImage('assets/carousel/brand_name.jpg'),
                        fit: BoxFit.fitWidth)),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    image: DecorationImage(
                        image: AssetImage(
                            'assets/carousel/social_media_template.jpg'),
                        fit: BoxFit.fitWidth)),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    image: DecorationImage(
                        image:
                            AssetImage('assets/carousel/banner_template.jpg'),
                        fit: BoxFit.fitWidth)),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    image: DecorationImage(
                        image: AssetImage('assets/carousel/identity_card.jpg'),
                        fit: BoxFit.fitWidth)),
              )
            ],
            options: CarouselOptions(
              height: 200,
              aspectRatio: 10 / 5,
              viewportFraction: 0.9,
              initialPage: 0,
              enableInfiniteScroll: true,
              reverse: false,
              autoPlay: true,
              autoPlayInterval: Duration(seconds: 4),
              autoPlayAnimationDuration: Duration(milliseconds: 1000),
              autoPlayCurve: Curves.fastOutSlowIn,
              enlargeCenterPage: true,
              onPageChanged: callbackFunction,
              scrollDirection: Axis.horizontal,
            )),
        new GridDashboard(),
        // new ListDashboard(
        //   gambar:
        //       "https://asset-a.grid.id/crop/0x0:0x0/750x504/photo/2021/04/24/cara-melestarikan-lingkunganjpg-20210424034755.jpg",
        //   judul:
        //       "Cara Melestarikan Lingkungan untuk Menjaga Alam di Sekitar Kita",
        //   link:
        //       "https://kids.grid.id/read/472665009/cara-melestarikan-lingkungan-untuk-menjaga-alam-di-sekitar-kita?page=all",
        //   deskripsi: desc,
        // ),
        // new ListDashboard(
        //   gambar:
        //       "https://cdn-image.hipwee.com/wp-content/uploads/2016/04/hipwee-alam-750x422.jpg",
        //   judul:
        //       "Tak Pernah Meminta Namun Selalu Memberi, Alam Adalah Sahabat Terbaik Kita",
        //   link:
        //       "https://www.hipwee.com/opini/alam-adalah-sahabat-terbaik-kita/",
        //   deskripsi: desc,
        // ),
        // new ListDashboard(
        //   gambar:
        //       "https://media.perjalanandunia.com/wp-content/uploads/2018/12/02131443/Pesona-Keindahan-Alam-Kosta-Rika-yang-Menakjubkan-1021x580.jpg",
        //   judul: "Pesona Keindahan Alam Kosta Rika yang Menakjubkan",
        //   link: "https://perjalanandunia.com/amerika-utara/alam-kosta-rika/",
        //   deskripsi: desc,
        // ),
        // new ListDashboard(
        //   gambar:
        //       "https://cdn0-production-images-kly.akamaized.net/gTA3BfVzseDAZz6ZNQz9vpB51ew=/640x360/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/2750752/original/076642300_1552532855-foto_HL.jpg",
        //   judul:
        //       "14 Wisata Alam Jogja dari Pantai Hingga Pegunungan, Asyik Buat Refreshing",
        //   link:
        //       "https://www.liputan6.com/lifestyle/read/3916498/14-wisata-alam-jogja-dari-pantai-hingga-pegunungan-asyik-buat-refreshing",
        //   deskripsi: desc,
        // ),
        // new ListDashboard(
        //   gambar:
        //       "https://cdns.klimg.com/dream.co.id/resources/news/2021/06/04/169792/664xauto-55-kata-kata-alam-yang-indah-dan-penuh-makna-tumbuhkan-rasa-syukur-210604l.jpg",
        //   judul:
        //       "70 Kata-Kata Alam yang Indah dan Penuh Makna, Tumbuhkan Rasa Syukur",
        //   link:
        //       "https://www.dream.co.id/your-story/55-kata-kata-alam-yang-indah-dan-penuh-makna-tumbuhkan-rasa-syukur-210604l.html",
        //   deskripsi: desc,
        // ),
        // new ListDashboard(
        //   gambar:
        //       "https://klikhijau.com/wp-content/uploads/2019/10/Wiratno-Konservasi-Alam-Bukan-Sekadar-Pekerjaan-Tapi-Jalan-Hidup.jpg",
        //   judul:
        //       "Fungsi Alam Semesta dan Cara Bijak bagi Manusia Menjaga Kebermanfaatannya",
        //   link:
        //       "https://klikhijau.com/read/fungsi-alam-semesta-dan-cara-bijak-bagi-manusia-menjaga-kebermanfaatannya/",
        //   deskripsi: desc,
        // ),
      ],
    );
  }
}

class ListDashboard extends StatelessWidget {
  ListDashboard({this.gambar, this.judul, this.deskripsi, this.link});
  final String gambar;
  final String judul;
  final String deskripsi;
  final String link;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 2.0, bottom: 2.0),
      child: Column(
        children: [
          Container(
            // width: 350,
            child: Card(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    width: MediaQuery.of(context).size.width,
                    height: 200,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage(gambar),
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(left: 15),
                    child: new InkWell(
                      child: Text(
                        judul,
                        style: Theme.of(context)
                            .textTheme
                            .headline6
                            .copyWith(color: Colors.black),
                      ),
                      onTap: () => launch(link),
                    ),
                  ),
                  Container(
                    constraints: BoxConstraints.expand(
                      height:
                          Theme.of(context).textTheme.headline4.fontSize * 1.1 +
                              70.0,
                    ),
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(left: 15, right: 15),
                    child: Text(
                      deskripsi,
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class GridDashboard extends StatelessWidget {
  // const GridDashboard({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView.count(
          crossAxisCount: 5,
          shrinkWrap: true,
          physics: ScrollPhysics(),
          mainAxisSpacing: 5,
          crossAxisSpacing: 5,
          children: [
            new GridContent(title: 'title 1', icon: Icons.access_alarm),
            new GridContent(
                title: 'title 2', icon: Icons.business_center_sharp),
            new GridContent(title: 'title 3', icon: Icons.calendar_today),
            new GridContent(
                title: 'title 4', icon: Icons.card_giftcard_outlined),
            new GridContent(
                title: 'title 5', icon: Icons.face_retouching_natural),
            new GridContent(title: 'title 6', icon: Icons.commute_rounded),
            new GridContent(title: 'title 7', icon: Icons.construction_sharp),
            new GridContent(title: 'title 8', icon: Icons.engineering_rounded),
            new GridContent(
                title: 'title 9', icon: Icons.fitness_center_rounded),
            new GridContent(title: 'title 10', icon: Icons.flight_outlined),
          ]),
    );
  }
}

class GridContent extends StatelessWidget {
  // const GridContent({ Key? key }) : super(key: key);
  final String title;
  final IconData icon;

  GridContent({@required this.title, @required this.icon});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        child: Container(
          width: 40,
          height: 40,
          child: Column(
            children: [
              Container(
                child: InkWell(
                  onTap: () {
                    showTopSnackBar(
                      context,
                      CustomSnackBar.error(
                        message: "$title, Sedang tahap pengembangan.",
                      ),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.all(3),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Center(
                          child: Container(
                            margin: EdgeInsets.only(top: 8),
                            child: new Icon(
                              icon,
                              color: Colors.deepOrange[800],
                              size: 30,
                            ),
                          ),
                        ),
                        Center(
                          child: Container(
                              margin: EdgeInsets.only(top: 8),
                              child: Text(
                                title,
                                style: TextStyle(
                                    fontSize: 13, fontWeight: FontWeight.w700),
                              )),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
