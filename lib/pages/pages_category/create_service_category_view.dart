import 'package:flutter/cupertino.dart';
import 'package:edesign/models/models_category/m_create_service_category.dart';
import 'package:edesign/repositories/repo_category/create_service_category_repository.dart';
import 'package:flutter/material.dart';

class CreateServiceCategoryView extends StatefulWidget {
  // const CreateServiceCategoryView({ Key? key }) : super(key: key);

  @override
  _CreateServiceCategoryViewState createState() =>
      _CreateServiceCategoryViewState();
}

class _CreateServiceCategoryViewState extends State<CreateServiceCategoryView> {
  final TextEditingController _controllerParent = TextEditingController();
  final TextEditingController _controllerName = TextEditingController();
  final TextEditingController _controllerDesc = TextEditingController();

  Future<CreateServiceCategory> _futureCreateServiceCategory;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Create Service Category"),
        ),
        body: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(8.0),
          child: (_futureCreateServiceCategory == null) ? buildColumn() : null,
        ),
      ),
    );
  }

  Column buildColumn() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        TextField(
          controller: _controllerParent,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(hintText: 'Enter Category'),
        ),
        TextField(
          controller: _controllerName,
          decoration: InputDecoration(hintText: 'Enter Name'),
        ),
        TextField(
          controller: _controllerDesc,
          decoration: InputDecoration(hintText: 'Enter Description'),
        ),
        ElevatedButton(
          onPressed: () {
            setState(() {
              _futureCreateServiceCategory =
                  CreateServiceCategoryRepository.createServiceCategory(
                      int.parse(_controllerParent.text),
                      _controllerName.text,
                      _controllerDesc.text);
            });
          },
          child: Text('Create Data'),
        ),
        ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text("Back"))
      ],
    );
  }
}
