import 'package:edesign/models/models_order/order.dart';
import 'package:edesign/models/models_printable/m_printable.dart';
import 'package:edesign/repositories/repo_order/read_order_item_repo.dart';
import 'package:edesign/repositories/repo_printable/printable_repository.dart';
import 'package:flutter/material.dart';
import 'package:edesign/helper/numberFormat.dart';

// ignore: must_be_immutable
class DetaileOrderView extends StatefulWidget {
  var id;
  // const DetaileOrderView({ Key? key }) : super(key: key);
  DetaileOrderView({this.id});
  @override
  _DetaileOrderViewState createState() => _DetaileOrderViewState();
}

class _DetaileOrderViewState extends State<DetaileOrderView> {
  Future<Order> _futureFetchOrderItemId;
  Future<List<Printable>> _printableF;

  @override
  void initState() {
    // path in repository/repo_order/read_order_item_repo.dart
    _futureFetchOrderItemId =
        ReadOrderItemRepository.fetchOrderItemId(widget.id);
    _printableF = PrintableRepository.getPrintables();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          elevation: 0,
          centerTitle: false,
          title: Text('Detail Pesanan'),
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back)),
          backgroundColor: Colors.blueAccent,
        ),
        body: SingleChildScrollView(
          child: Center(
            child: buildPage(),
          ),
        ),
      ),
    );
  }

  Widget buildPage() {
    return Container(
      alignment: Alignment.center,
      child: buildPageData(),
    );
  }

  Widget buildTextJudul() {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(top: 5.0, bottom: 8.0),
        child: Center(
          child: Text(
            "Detail Pesanan",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
          ),
        ),
      ),
    );
  }

  Widget buildPageData() {
    return FutureBuilder(
        future: _futureFetchOrderItemId,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.orderItems[0].printable == null) {
              return Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(top: 10.0),
                  child: Card(
                    child: Container(
                      // color: Colors.amber,
                      alignment: Alignment.center,
                      height: 350.0,
                      width: 350.0,
                      child: Column(
                        children: [
                          Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(top: 10.0),
                            width: 150.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Text(
                              'Detail Pesanan Anda',
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 20.0),
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 15.0),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: 10.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Customer',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, top: 15.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    snapshot.data.customer.toString(),
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 15.0),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: 10.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Service',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, top: 15.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    snapshot.data.orderItems[0].service['name']
                                            .toString() +
                                        " ( " +
                                        snapshot
                                            .data.orderItems[0].service['price']
                                            .toString() +
                                        " )",
                                    style: TextStyle(fontSize: 16.0),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 15.0),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: 10.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Printable',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 20.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text("-"),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 15.0),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: 10.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Finishing',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, top: 15.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text("-"),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  padding:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Qty',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, top: 15.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    snapshot.data.orderItems[0].quantity
                                        .toString(),
                                    style: TextStyle(fontSize: 16.0),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  padding:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Total Price',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, top: 15.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    formatNumber(
                                            snapshot.data.orderItems[0].price)
                                        .toString(),
                                    style: TextStyle(fontSize: 16.0),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ));
            } else if (snapshot.data.orderItems[0].finishing == null) {
              return Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(top: 10.0),
                  child: Card(
                    child: Container(
                      // color: Colors.amber,
                      alignment: Alignment.center,
                      height: 350.0,
                      width: 350.0,
                      child: Column(
                        children: [
                          Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(top: 10.0),
                            width: 150.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Text(
                              'Detail Pesanan Anda',
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 20.0),
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 15.0),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: 10.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Customer',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, top: 15.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    snapshot.data.customer.toString(),
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 15.0),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: 10.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Service',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, top: 15.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    snapshot.data.orderItems[0].service['name']
                                            .toString() +
                                        " ( " +
                                        formatNumber(snapshot.data.orderItems[0]
                                                .service['price'])
                                            .toString() +
                                        " )",
                                    style: TextStyle(fontSize: 16.0),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 15.0),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: 10.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Printable',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 20.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    snapshot.data.orderItems[0]
                                            .printable['name']
                                            .toString() +
                                        " ( " +
                                        formatNumber(snapshot.data.orderItems[0]
                                                .printable['price'])
                                            .toString() +
                                        " )",
                                    style: TextStyle(fontSize: 16.0),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 15.0),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: 10.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Finishing',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, top: 15.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text("-"),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  padding:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Qty',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, top: 15.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    snapshot.data.orderItems[0].quantity
                                        .toString(),
                                    style: TextStyle(fontSize: 16.0),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  padding:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Total Price',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, top: 15.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    formatNumber(
                                            snapshot.data.orderItems[0].price)
                                        .toString(),
                                    style: TextStyle(fontSize: 16.0),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ));
            } else {
              return Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(top: 10.0),
                  child: Card(
                    child: Container(
                      // color: Colors.amber,
                      alignment: Alignment.center,
                      height: 350.0,
                      width: 350.0,
                      child: Column(
                        children: [
                          Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(top: 10.0),
                            width: 150.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Text(
                              'Detail Pesanan Anda',
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 20.0),
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 15.0),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: 10.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Customer',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, top: 15.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    snapshot.data.customer.toString(),
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 15.0),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: 10.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Service',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, top: 15.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    snapshot.data.orderItems[0].service['name']
                                            .toString() +
                                        " ( " +
                                        formatNumber(snapshot.data.orderItems[0]
                                                .service['price'])
                                            .toString() +
                                        " )",
                                    style: TextStyle(fontSize: 16.0),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 15.0),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: 10.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Printable',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 20.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    snapshot.data.orderItems[0]
                                            .printable['name']
                                            .toString() +
                                        " ( " +
                                        formatNumber(snapshot.data.orderItems[0]
                                                .printable['price'])
                                            .toString() +
                                        " )",
                                    style: TextStyle(fontSize: 16.0),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 15.0),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: 10.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Finishing',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, top: 15.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    snapshot.data.orderItems[0]
                                            .finishing['name']
                                            .toString() +
                                        " ( " +
                                        formatNumber(snapshot.data.orderItems[0]
                                                .finishing['price'])
                                            .toString() +
                                        " )",
                                    style: TextStyle(fontSize: 16.0),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  padding:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Qty',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, top: 15.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    snapshot.data.orderItems[0].quantity
                                        .toString(),
                                    style: TextStyle(fontSize: 16.0),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 350.0,
                            height: 30.0,
                            // color: Colors.blueAccent,
                            child: Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  padding:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  width: 130.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    'Total Price',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, top: 15.0),
                                  alignment: Alignment.center,
                                  width: 10.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    ':',
                                    style: TextStyle(fontSize: 16.0),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.only(left: 10.0, top: 15.0),
                                  padding: EdgeInsets.only(left: 10.0),
                                  alignment: Alignment.centerLeft,
                                  width: 165.0,
                                  height: 30.0,
                                  // color: Colors.pink,
                                  child: Text(
                                    formatNumber(
                                            snapshot.data.orderItems[0].price)
                                        .toString(),
                                    style: TextStyle(fontSize: 16.0),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ));
            }
          }
          return CircularProgressIndicator();
        });
  }

  Widget dataPrintable() {
    return FutureBuilder<List<Printable>>(
        future: _printableF,
        builder: (context, snapshot) {
          // List<P>
          if (snapshot.hasData) {
            List<Printable> x = snapshot.data;
            // return Text(x)
            return ListView.builder(
                itemCount: x.length,
                itemBuilder: (c, i) {
                  return Text(x[i].price.toString());
                });
          }
          return CircularProgressIndicator();
        });
  }
}
