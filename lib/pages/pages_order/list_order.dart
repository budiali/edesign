import 'package:edesign/models/models_order/order.dart';
import 'package:edesign/repositories/repo_order/get_order_item.dart';
import 'package:flutter/material.dart';
import 'package:card_settings/card_settings.dart';
import 'package:edesign/helper/numberFormat.dart';

class ListOrder extends StatefulWidget {
  // const ListOrder({ Key? key }) : super(key: key);
  final String id;
  ListOrder({this.id});
  @override
  _ListOrderState createState() => _ListOrderState();
}

class _ListOrderState extends State<ListOrder> {
  Future<Order> _future;
  @override
  void initState() {
    super.initState();
    _future = GetOrderItemRepository.getOrderItem(widget.id);
    // print(_future);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Detail Pesanan'),
          centerTitle: false,
          backgroundColor: Colors.blueAccent,
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back)),
        ),
        body: Center(
          child: buildPage(),
        ),
      ),
    );
  }

  Widget buildPage() {
    return FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.none &&
              snapshot.hasData == null) {
            print('data not found!');
          }
          if (snapshot.hasData) {
            if (snapshot.data.orderItems[0].printable == null) {
              return Container(
                alignment: Alignment.topCenter,
                margin: EdgeInsets.only(top: 10.0),
                child: CardSettings(
                  children: [
                    CardSettingsSection(
                      header: CardSettingsHeader(
                        child: Container(
                          height: 80,
                          child: Row(
                            children: [
                              Expanded(
                                  child: Divider(
                                      color: Colors.purple, thickness: 1)),
                              Text('Detail Pesanan',
                                  style: TextStyle(fontSize: 20)),
                              Expanded(
                                  child: Divider(
                                      color: Colors.purple, thickness: 1)),
                            ],
                          ),
                        ),
                      ),
                      children: [
                        // CardSettingsText(
                        //   label: 'service',
                        //   initialValue:
                        //       snapshot.data.orderItems[0].service.toString(),
                        // ),
                        CardSettingsInstructions(
                          text: "Service : " +
                              snapshot.data.orderItems[0].service['name']
                                  .toString() +
                              " ( " +
                              formatNumber(snapshot
                                      .data.orderItems[0].service['price'])
                                  .toString() +
                              " )",
                          textColor: Colors.black,
                          visible: true,
                        ),
                        CardSettingsInstructions(
                          text: "Status : " + snapshot.data.status.toString(),
                          textColor: Colors.black,
                          visible: true,
                        ),
                        CardSettingsInstructions(
                          text: "Printable : -",
                          textColor: Colors.black,
                          visible: true,
                        ),
                        CardSettingsInstructions(
                          text: "Finishing : -",
                          textColor: Colors.black,
                          visible: true,
                        ),
                        CardSettingsInstructions(
                          text: "Quantity : " +
                              snapshot.data.orderItems[0].quantity.toString(),
                          textColor: Colors.black,
                          visible: true,
                        ),
                        CardSettingsInstructions(
                          text: "Price : " +
                              formatNumber(snapshot.data.orderItems[0].price)
                                  .toString(),
                          textColor: Colors.black,
                          visible: true,
                        )
                      ],
                    ),
                  ],
                ),
              );
            } else if (snapshot.data.orderItems[0].finishing == null) {
              return Container(
                alignment: Alignment.topCenter,
                margin: EdgeInsets.only(top: 10.0),
                child: CardSettings(
                  children: [
                    CardSettingsSection(
                      header: CardSettingsHeader(
                        child: Container(
                          height: 80,
                          child: Row(
                            children: [
                              Expanded(
                                  child: Divider(
                                      color: Colors.purple, thickness: 1)),
                              Text('Detail Pesanan',
                                  style: TextStyle(fontSize: 20)),
                              Expanded(
                                  child: Divider(
                                      color: Colors.purple, thickness: 1)),
                            ],
                          ),
                        ),
                      ),
                      children: [
                        // CardSettingsText(
                        //   label: 'service',
                        //   initialValue:
                        //       snapshot.data.orderItems[0].service.toString(),
                        // ),
                        CardSettingsInstructions(
                          text: "Service : " +
                              snapshot.data.orderItems[0].service['name']
                                  .toString() +
                              " ( " +
                              formatNumber(snapshot
                                      .data.orderItems[0].service['price'])
                                  .toString() +
                              " )",
                          textColor: Colors.black,
                          visible: true,
                        ),
                        CardSettingsInstructions(
                          text: "Status : " + snapshot.data.status.toString(),
                          textColor: Colors.black,
                          visible: true,
                        ),
                        CardSettingsInstructions(
                          text: "Printable : " +
                              snapshot.data.orderItems[0].printable['name']
                                  .toString() +
                              " ( " +
                              formatNumber(snapshot
                                      .data.orderItems[0].printable['price'])
                                  .toString() +
                              " )",
                          textColor: Colors.black,
                          visible: true,
                        ),
                        CardSettingsInstructions(
                          text: "Finishing : -",
                          textColor: Colors.black,
                          visible: true,
                        ),
                        CardSettingsInstructions(
                          text: "Quantity : " +
                              snapshot.data.orderItems[0].quantity.toString(),
                          textColor: Colors.black,
                          visible: true,
                        ),
                        CardSettingsInstructions(
                          text: "Price : " +
                              formatNumber(snapshot.data.orderItems[0].price)
                                  .toString(),
                          textColor: Colors.black,
                          visible: true,
                        )
                      ],
                    ),
                  ],
                ),
              );
            } else {
              return Container(
                alignment: Alignment.topCenter,
                margin: EdgeInsets.only(top: 10.0),
                child: CardSettings(
                  children: [
                    CardSettingsSection(
                      header: CardSettingsHeader(
                        child: Container(
                          height: 80,
                          child: Row(
                            children: [
                              Expanded(
                                  child: Divider(
                                      color: Colors.purple, thickness: 1)),
                              Text('Detail Pesanan',
                                  style: TextStyle(fontSize: 20)),
                              Expanded(
                                  child: Divider(
                                      color: Colors.purple, thickness: 1)),
                            ],
                          ),
                        ),
                      ),
                      children: [
                        // CardSettingsText(
                        //   label: 'service',
                        //   initialValue:
                        //       snapshot.data.orderItems[0].service.toString(),
                        // ),
                        CardSettingsInstructions(
                          text: "Service : " +
                              snapshot.data.orderItems[0].service['name']
                                  .toString() +
                              " ( " +
                              formatNumber(snapshot
                                      .data.orderItems[0].service['price'])
                                  .toString() +
                              " )",
                          textColor: Colors.black,
                          visible: true,
                        ),
                        CardSettingsInstructions(
                          text: "Status : " + snapshot.data.status.toString(),
                          textColor: Colors.black,
                          visible: true,
                        ),
                        CardSettingsInstructions(
                          text: "Printable : " +
                              snapshot.data.orderItems[0].printable['name']
                                  .toString() +
                              " ( " +
                              formatNumber(snapshot
                                      .data.orderItems[0].printable['price'])
                                  .toString() +
                              " )",
                          textColor: Colors.black,
                          visible: true,
                        ),
                        CardSettingsInstructions(
                          text: "Finishing : " +
                              snapshot.data.orderItems[0].finishing['name']
                                  .toString() +
                              " ( " +
                              formatNumber(snapshot
                                      .data.orderItems[0].finishing['price'])
                                  .toString() +
                              " )",
                          textColor: Colors.black,
                          visible: true,
                        ),
                        CardSettingsInstructions(
                          text: "Quantity : " +
                              snapshot.data.orderItems[0].quantity.toString(),
                          textColor: Colors.black,
                          visible: true,
                        ),
                        CardSettingsInstructions(
                          text: "Total Price : " +
                              formatNumber(snapshot.data.orderItems[0].price)
                                  .toString(),
                          textColor: Colors.black,
                          visible: true,
                        )
                      ],
                    ),
                  ],
                ),
              );
            }
          }
          return CircularProgressIndicator();
        });
  }

  Widget buildJudulShowOrder() {
    return Container(
      alignment: Alignment.topCenter,
      child: SizedBox(
          width: 200.0,
          height: 20.0,
          child: DecoratedBox(
            decoration: BoxDecoration(
              color: Colors.grey[400],
            ),
            child: Center(
              child: Text(
                "Data Order",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
              ),
            ),
          )),
    );
  }
}
