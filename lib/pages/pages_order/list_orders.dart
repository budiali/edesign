import 'package:edesign/models/models_order/order.dart';
import 'package:edesign/repositories/repo_order/get_order_item.dart';
import 'package:edesign/pages/pages_order/list_order.dart';
import 'package:flutter/material.dart';

class ListOrders extends StatefulWidget {
  // const ListOrders({ Key? key }) : super(key: key);

  @override
  _ListOrdersState createState() => _ListOrdersState();
}

class _ListOrdersState extends State<ListOrders> {
  Future<List<Order>> _future;

  @override
  void initState() {
    _future = GetOrderItemRepository.getOrder();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Daftar Pesanan'),
          centerTitle: false,
          elevation: 0,
          backgroundColor: Colors.blueAccent,
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back)),
        ),
        body: Center(
          child: buildPageListOrders(),
        ),
      ),
    );
  }

  Widget buildPageListOrders() {
    return FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          List<Order> listOrders = snapshot.data;
          if (snapshot.connectionState == ConnectionState.none &&
              snapshot.hasData == null) {
            print('Data : ${snapshot.data}');
          }
          if (listOrders == null) {
            print('Kamu belum memiliki pesanan...');
          } else {
            return ListView.builder(
                itemCount: listOrders.length,
                itemBuilder: (context, index) {
                  return Card(
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          ListTile(
                            leading: Icon(Icons.local_grocery_store_outlined),
                            title: Text(listOrders[index].customer),
                            subtitle: Text("Being Processed..."),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              TextButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ListOrder(
                                                id: listOrders[index]
                                                    .id
                                                    .toString(),
                                              )));
                                },
                                // child: Text('Lihat Pesanan')
                                child: Row(
                                  children: [
                                    Icon(Icons.remove_red_eye),
                                    SizedBox(
                                      width: 5.0,
                                    ),
                                    Text('Lihat Pesanan')
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                });
          }
          return CircularProgressIndicator();
        });
  }
}
