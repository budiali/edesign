import 'package:edesign/helper/numberFormat.dart';
import 'package:edesign/models/models_category/category.dart';
import 'package:edesign/models/models_service/get_service.dart';
import 'package:edesign/models/models_wallet/m_wallet.dart';
import 'package:edesign/pages/pages_service.dart/detail_service_view.dart';
import 'package:edesign/repositories/repo_wallet/wallet_repository.dart';
import 'package:flutter/material.dart';
import 'package:edesign/repositories/repo_category/category_repository.dart';
import 'package:edesign/repositories/repo_service/get_service_repo.dart';
import 'dart:async';

class TampilCategory extends StatefulWidget {
  // const TampilCategory({ Key? key }) : super(key: key);

  @override
  _TampilCategoryState createState() => _TampilCategoryState();
}

class _TampilCategoryState extends State<TampilCategory> {
  Future<List<GetServiceCategories>> _future;
  Future<List<GetService>> _futureGetService;
  List<GetServiceCategories> listCategories;
  Future<Wallet> _futureWallet;

  // Future<ReadServiceCategory> _futureReadServiceCategory;
  // Future<ReadService> _futureReadService;

  TabController _tabController;

  List<Tab> tabs = <Tab>[];
  List<Widget> _generalWidgets = <Widget>[];

  @override
  void initState() {
    super.initState();
    _future = CategoryRepository.getCategories();
  }

  // @override
  // void dispose() {
  //   _tabController.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          listCategories = snapshot.data;

          if (snapshot.hasData) {
            for (int i = 0; i < snapshot.data.length; i++) {
              tabs.add(Tab(
                child: Text(
                  snapshot.data[i].name,
                  style: TextStyle(color: Colors.white, fontSize: 17.0),
                ),
              ));
            }

            return DefaultTabController(
              length: listCategories.length != null ? listCategories.length : 1,
              child: MaterialApp(
                home: Scaffold(
                  // drawer: makeDrawer(),
                  appBar: AppBar(
                    centerTitle: false,
                    title: Text('Kategori & Servis'),
                    backgroundColor: Colors.blueAccent,
                    elevation: 0,
                    leading: IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: Icon(Icons.arrow_back)),
                    bottom: TabBar(controller: _tabController, tabs: tabs),
                  ),
                  body: TabBarView(
                      controller: _tabController, children: getWidgets()),
                  floatingActionButton: FloatingActionButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.home_filled),
                    backgroundColor: Colors.lightBlue,
                  ),
                ),
              ),
            );
          }

          if (snapshot.hasError) print(snapshot.error.toString());
          return Scaffold(
            body: Center(
                child: Text(snapshot.hasError
                    ? snapshot.error.toString()
                    : "Sedang menyiapkan data...")),
          );
        });
  }

  Widget showGetService(String categoryId) {
    _futureGetService = GetServiceRepository.getService(categoryId);
    print(_futureGetService);
    return FutureBuilder(
        future: _futureGetService,
        builder: (context, snapshot) {
          List<GetService> listService = snapshot.data;
          if (snapshot.connectionState == ConnectionState.none &&
              snapshot.hasData == null) {
            print('data snapshot ${snapshot.data}');
          }
          return GridView.builder(
              // crossAxisCount: 2,
              physics: ScrollPhysics(),
              shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                // childAspectRatio: (3 / 2),
              ),
              itemCount: listService == null ? 0 : listService.length,
              itemBuilder: (context, i) {
                return Card(
                    // padding: EdgeInsets.all(10.0),
                    child: Wrap(
                  children: [
                    Center(
                      child: new Container(
                        width: MediaQuery.of(context).size.width,
                        height: 120,
                        decoration: new BoxDecoration(
                            image: new DecorationImage(
                                image: new NetworkImage(
                                    'https://img.freepik.com/free-vector/web-design-concept-with-flat-style_23-2147854553.jpg?size=338&ext=jpg'),
                                fit: BoxFit.fill)),
                      ),
                    ),
                    new Container(
                      // color: Colors.deepOrange,
                      width: 200,
                      // height: 30,
                      padding: EdgeInsets.all(5.0),
                      child: Text(
                        listService[i].name.toString(),
                        style:
                            TextStyle(fontFamily: "Consolas", fontSize: 15.0),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                    ),
                    Row(
                      children: [
                        Flexible(
                          child: Container(
                            // color: Colors.amber,
                            margin: EdgeInsets.only(left: 5),
                            alignment: Alignment.center,
                            child: Text(
                                formatNumber(listService[i].price).toString()),
                          ),
                        ),
                        Flexible(
                          child: Container(
                            // color: Colors.yellow,
                            margin: EdgeInsets.only(left: 10),
                            alignment: Alignment.center,
                            child: TextButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              DetailServiceView(
                                                id: listService[i]
                                                    .id
                                                    .toString(),
                                              )));
                                },
                                child: Text('Detail Service')),
                          ),
                        ),
                      ],
                    ),
                  ],
                ));
              });
        });
  }

  List<Widget> getWidgets() {
    _generalWidgets.clear();
    for (int i = 0; i < tabs.length; i++) {
      _generalWidgets.add(showGetService(listCategories[i].id.toString()));
    }
    return _generalWidgets;
  }
}
