import 'dart:convert' show ascii, base64, json;
// import 'package:carousel_slider/carousel_slider.dart';git
import 'package:edesign/category_list_view.dart';
import 'package:edesign/models/models_category/category.dart';
// import 'package:edesign/models/create_service.dart';
import 'package:edesign/pages/LoginPage.dart';
import 'package:edesign/pages/course_info_screen.dart';
import 'package:edesign/pages/dashboard.dart';
import 'package:edesign/pages/pages_order/list_orders.dart';
import 'package:edesign/pages/pages_wallet/show_wallet.dart';
import 'package:edesign/pages/popular_course_list_view.dart';
import 'package:edesign/pages/pages_service.dart/create_service_view.dart';
import 'package:edesign/pages/pages_category/create_service_category_view.dart';
import 'package:edesign/main.dart';
import 'package:edesign/pages/tampilCategory.dart';
import 'package:flutter/material.dart';
import 'package:edesign/app_theme.dart';
// import 'package:http/http.dart' as http;
import 'package:edesign/repositories/repo_category/category_repository.dart';
// import 'package:edesign/repositories/read_service_category_repository.dart';
// import 'package:edesign/repositories/read_service_repository.dart';
// import 'package:edesign/repositories/create_service_repository.dart';

import 'dart:async';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

// final storage = new FlutterSecureStorage();

class HomePage extends StatefulWidget {
  HomePage(this.token, this.payload);

  factory HomePage.tokenObject(Map<String, dynamic> token) => HomePage(
      token,
      json.decode(ascii.decode(
          base64.decode(base64.normalize(token['access'].split(".")[1])))));

  final Map<String, dynamic> token;
  final Map<String, dynamic> payload;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  CategoryType categoryType = CategoryType.ui;
  Future<List<GetServiceCategories>> _future;
  List<GetServiceCategories> listCategories;
  // Future<ReadServiceCategory> _futureReadServiceCategory;
  // Future<ReadService> _futureReadService;

  // ignore: deprecated_member_use
  List<Tab> tabs = List<Tab>();

  get callbackFunction => null;

  @override
  void initState() {
    super.initState();
    _future = CategoryRepository.getCategories();
    // _futureReadServiceCategory =
    //     ReadServiceCategoryRepository.fetchServiceCategoryRead(
    //         widget.token["access"]);
    // _futureReadService = ReadServiceRepository.fetchReadService();
  }

  int currentIndex = 0;
  void changePage(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  // @override
  // void dispose() {
  //   _tabController.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text('Home'),
        centerTitle: false,
        backgroundColor: Colors.blueAccent,
        actions: [
          IconButton(
            icon: const Icon(Icons.shopping_cart),
            tooltip: 'Open Design List',
            onPressed: () {
              // handle the press
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => TampilCategory()));
            },
          ),
        ],
      ),
      body: SafeArea(
          child: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Column(
          children: [
            Dashboard(),
          ],
        ),
      )),
      drawer: makeDrawer(),
    );
  }

  // Example Builder for Service/Category/{id}
  // Widget showReadServiceCategory() {
  // return FutureBuilder(
  //     future: _futureReadServiceCategory,
  //     builder: (context, snapshot) {
  //       if (snapshot.hasData) {
  //         return Text(snapshot.data.name);
  //       } else if (snapshot.hasError) {
  //         return Text("${snapshot.error}");
  //       }
  //       // By default, show a loading spinner.
  //       return CircularProgressIndicator();
  //     });
  // }

  // Widget tabBar() {
  //   return DefaultTabController(
  //     length: 3,
  //     // child: child
  //     child: Scaffold(
  //       appBar: AppBar(
  //         backgroundColor: Colors.white,
  //         elevation: 0,
  //         bottom: TabBar(
  //             unselectedLabelColor: Colors.redAccent,
  //             indicatorSize: TabBarIndicatorSize.label,
  //             indicator: BoxDecoration(
  //               borderRadius: BorderRadius.circular(50),
  //               color: Colors.redAccent,
  //             ),
  //             tabs: [
  //               Tab(
  //                 child: Container(
  //                   decoration: BoxDecoration(
  //                       borderRadius: BorderRadius.circular(50),
  //                       border: Border.all(color: Colors.redAccent, width: 1)),
  //                   child: Align(
  //                     alignment: Alignment.center,
  //                     child: Text("Category 1"),
  //                   ),
  //                 ),
  //               ),
  //               Tab(
  //                 child: Container(
  //                   decoration: BoxDecoration(
  //                       borderRadius: BorderRadius.circular(50),
  //                       border: Border.all(color: Colors.redAccent, width: 1)),
  //                   child: Align(
  //                     alignment: Alignment.center,
  //                     child: Text("Category 2"),
  //                   ),
  //                 ),
  //               ),
  //               Tab(
  //                 child: Container(
  //                   decoration: BoxDecoration(
  //                       borderRadius: BorderRadius.circular(50),
  //                       border: Border.all(color: Colors.redAccent, width: 1)),
  //                   child: Align(
  //                     alignment: Alignment.center,
  //                     child: Text("Category 3"),
  //                   ),
  //                 ),
  //               ),
  //             ]),
  //       ),
  //       body: TabBarView(children: [
  //         Icon(Icons.apps),
  //         Icon(Icons.movie),
  //         Icon(Icons.games),
  //       ]),
  //     ),
  //   );
  // }

  Widget showGetServiceCategory() {
    return FutureBuilder(
      future: _future,
      builder: (context, snapshot) {
        List<GetServiceCategories> list = snapshot.data;
        if (snapshot.connectionState == ConnectionState.none &&
            snapshot.hasData == null) {
          print('data snapshot proyek adalah: ${snapshot.data} ');
          return Container();
        }
        return ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: list.length,
          itemBuilder: (context, index) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 5, right: 6),
                  child: Row(
                    children: [
                      ElevatedButton(
                          onPressed: null,
                          child: Text(list[index].name.toString())),
                    ],
                  ),
                ),
              ],
            );
          },
        );
      },
    );
  }

  // Widget showReadService() {
  //   return FutureBuilder(
  //       future: _futureReadService,
  //       builder: (context, snapshot) {
  //         if (snapshot.hasData) {
  //           // return Column(
  //           //   mainAxisAlignment: MainAxisAlignment.center,
  //           //   children: [
  //           //     Text(snapshot.data.name),
  //           //     TextField(
  //           //       // controller: _controller,
  //           //       decoration: InputDecoration(hintText: 'Enter Name'),
  //           //     ),
  //           //     RaisedButton(
  //           //       child: Text('Update Data'),
  //           //       onPressed: null,
  //           //       // onPressed: () {
  //           //       //   setState(() {
  //           //       //     // _futureReadService = updateAlbum(_controller.text);
  //           //       //   });
  //           //       // },
  //           //     ),
  //           //   ],
  //           // );
  //           return Text(snapshot.data.name);
  //         } else if (snapshot.hasError) {
  //           return Text("${snapshot.error}");
  //         }
  //         // By default, show a loading spinner.
  //         return CircularProgressIndicator();
  //       });
  // }

  Widget getCategoryUI() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16),
          child: Text(
            'Category',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 22,
              letterSpacing: 0.27,
              color: DesignCourseAppTheme.darkerText,
            ),
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 16, right: 16),
          child: Row(
            children: <Widget>[
              getButtonUI(CategoryType.ui, categoryType == CategoryType.ui),
              const SizedBox(
                width: 16,
              ),
              getButtonUI(
                  CategoryType.coding, categoryType == CategoryType.coding),
              const SizedBox(
                width: 16,
              ),
              getButtonUI(
                  CategoryType.basic, categoryType == CategoryType.basic),
            ],
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        CategoryListView(
          callBack: () {
            moveTo();
          },
        ),
      ],
    );
  }

  Widget getPopularCourseUI() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Popular Course',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 22,
              letterSpacing: 0.27,
              color: DesignCourseAppTheme.darkerText,
            ),
          ),
          Flexible(
            child: PopularCourseListView(
              callBack: () {
                moveTo();
              },
            ),
          )
        ],
      ),
    );
  }

  void moveTo() {
    Navigator.push<dynamic>(
      context,
      MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => CourseInfoScreen(),
      ),
    );
  }

  Widget getButtonUI(CategoryType categoryTypeData, bool isSelected) {
    String txt = '';
    // FutureBuilder(
    //   future: _future,
    //   builder: (context, snapshot) {
    //     List<GetServiceCategories> list = snapshot.data;
    //     if (snapshot.connectionState == ConnectionState.none &&
    //         snapshot.hasData == null) {}
    //     return ListView.builder(
    //       scrollDirection: Axis.horizontal,
    //       itemCount: list == null ? 0 : list.length,
    //       itemBuilder: (context, index) {
    //         return Text(list[0].name.toString());
    //       },
    //     );
    //   },
    // );

    if (CategoryType.ui == categoryTypeData) {
      txt = 'Ui/Ux';
    } else if (CategoryType.coding == categoryTypeData) {
      txt = 'Coding';
    } else if (CategoryType.basic == categoryTypeData) {
      txt = 'Basic UI';
    }

    return Expanded(
      child: Container(
        decoration: BoxDecoration(
            color: isSelected
                ? DesignCourseAppTheme.nearlyBlue
                : DesignCourseAppTheme.nearlyWhite,
            borderRadius: const BorderRadius.all(Radius.circular(24.0)),
            border: Border.all(color: DesignCourseAppTheme.nearlyBlue)),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            splashColor: Colors.white24,
            borderRadius: const BorderRadius.all(Radius.circular(24.0)),
            onTap: () {
              setState(() {
                categoryType = categoryTypeData;
              });
            },
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 12, bottom: 12, left: 18, right: 18),
              child: Center(
                child: Text(
                  txt,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 12,
                    letterSpacing: 0.27,
                    color: isSelected
                        ? DesignCourseAppTheme.nearlyWhite
                        : DesignCourseAppTheme.nearlyBlue,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget getSearchBarUI() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 18),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * 0.75,
            height: 64,
            child: Padding(
              padding: const EdgeInsets.only(top: 8, bottom: 8),
              child: Container(
                decoration: BoxDecoration(
                  color: HexColor('#F8FAFB'),
                  borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(13.0),
                    bottomLeft: Radius.circular(13.0),
                    topLeft: Radius.circular(13.0),
                    topRight: Radius.circular(13.0),
                  ),
                ),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.only(left: 16, right: 16),
                        child: TextFormField(
                          style: TextStyle(
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: DesignCourseAppTheme.nearlyBlue,
                          ),
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            labelText: 'Search for course',
                            border: InputBorder.none,
                            helperStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              color: HexColor('#B9BABC'),
                            ),
                            labelStyle: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              letterSpacing: 0.2,
                              color: HexColor('#B9BABC'),
                            ),
                          ),
                          onEditingComplete: () {},
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 60,
                      height: 60,
                      child: Icon(Icons.search, color: HexColor('#B9BABC')),
                    )
                  ],
                ),
              ),
            ),
          ),
          const Expanded(
            child: SizedBox(),
          )
        ],
      ),
    );
  }

  Widget getAppBarUI() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 18, right: 18),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Choose your',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    letterSpacing: 0.2,
                    color: DesignCourseAppTheme.grey,
                  ),
                ),
                Text(
                  'Design Course',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 22,
                    letterSpacing: 0.27,
                    color: DesignCourseAppTheme.darkerText,
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: 60,
            height: 60,
            child: Image.asset('assets/design_course/userImage.png'),
          )
        ],
      ),
    );
  }

  Widget createService() {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: ElevatedButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => CreateServiceView()));
          },
          child: Text("Create Service")),
    );
  }

  Widget createServiceCategory() {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: ElevatedButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => CreateServiceCategoryView()));
        },
        child: Text("Create Service Category"),
      ),
    );
  }

  Widget makeDrawer() {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
              decoration: BoxDecoration(
                  color: Colors.blueAccent,
                  image: DecorationImage(
                      image: AssetImage('assets/carousel/pic_drawer.jpeg'),
                      fit: BoxFit.fitWidth)),
              child: Text(
                'Drawer Header',
                style: TextStyle(color: Colors.white),
              )),
          ListTile(
            leading: Icon(Icons.account_balance_wallet_outlined),
            title: Text('Wallet'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ShowWallet()));
            },
          ),
          ListTile(
            leading: Icon(Icons.checklist_rounded),
            title: Text('Daftar Pesanan'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ListOrders()));
            },
          ),
          ListTile(
            leading: Icon(Icons.markunread_rounded),
            title: Text('Inbox'),
            subtitle: Text(
              'Sorry, the feature is under development.',
              style: TextStyle(
                fontStyle: FontStyle.italic,
              ),
            ),
            onTap: () {
              // code here
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.message_rounded),
            title: Text('Chat'),
            subtitle: Text(
              'Sorry, the feature is under development.',
              style: TextStyle(
                fontStyle: FontStyle.italic,
              ),
            ),
            onTap: () {
              // code here
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.logout_outlined),
            title: Text('Logout'),
            onTap: () {
              // code here
              // setState(() {
              //   Navigator.push(context,
              //       MaterialPageRoute(builder: (context) => LoginPage()));
              // });
              logoutAction();
            },
          ),
        ],
      ),
    );
  }

  final _storage = new FlutterSecureStorage();
  void logoutAction() async {
    // var x = await _storage.delete(key: 'refresh');
    var x = await _storage.delete(key: 'access');
    setState(() {
      Navigator.push(
              context, MaterialPageRoute(builder: (context) => LoginPage()))
          .then((value) => x);
      // isLoggedIn = false;
      // isBusy = false;
    });
  }
}

enum CategoryType { ui, coding, basic }
