class CreateOrder {
  int id;
  String customer;
  int partner;
  int paymentType;

  CreateOrder({this.id, this.customer, this.partner, this.paymentType});

  factory CreateOrder.fromJson(Map<String, dynamic> json) {
    return CreateOrder(
        id: json['id'] as int,
        customer: json['customer'] as String,
        partner: json['partner'] as int,
        paymentType: json['payment_type'] as int);
  }
}
