class OrderItem {
  int id;
  int order;
  int service;
  int quantity;
  int printable;
  int finishing;
  double price;

  OrderItem(
      {this.id,
      this.order,
      this.service,
      this.quantity,
      this.printable,
      this.finishing,
      this.price});

  factory OrderItem.fromJson(Map<String, dynamic> json) {
    return OrderItem(
        id: json["id"] as int,
        order: json["order"] as int,
        service: json["service"] as int,
        quantity: json["quantity"] as int,
        printable: json["printable"] as int,
        finishing: json["finishing"] as int,
        price: json["price"] as double);
  }
}
