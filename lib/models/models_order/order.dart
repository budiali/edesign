class Order {
  int id;
  String uuid;
  String customer;
  String partner;
  String status;
  int paymentType;
  List<OrderItems> orderItems;

  Order(
      {this.id,
      this.uuid,
      this.customer,
      this.partner,
      this.status,
      this.paymentType,
      this.orderItems});

  Order.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    uuid = json['uuid'];
    customer = json['customer'];
    partner = json['partner'];
    status = json['status'];
    paymentType = json['payment_type'];
    if (json['order_items'] != null) {
      orderItems = <OrderItems>[];
      json['order_items'].forEach((v) {
        orderItems.add(new OrderItems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['uuid'] = this.uuid;
    data['customer'] = this.customer;
    data['partner'] = this.partner;
    data['status'] = this.status;
    data['payment_type'] = this.paymentType;
    if (this.orderItems != null) {
      data['order_items'] = this.orderItems.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderItems {
  int order;
  dynamic service;
  dynamic printable;
  dynamic finishing;
  int quantity;
  double price;

  OrderItems(
      {this.order,
      this.service,
      this.printable,
      this.finishing,
      this.quantity,
      this.price});

  OrderItems.fromJson(Map<String, dynamic> json) {
    order = json['order'];
    service = json['service'];
    printable = json['printable'];
    finishing = json['finishing'];
    quantity = json['quantity'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['order'] = this.order;
    data['service'] = this.service;
    data['printable'] = this.printable;
    data['finishing'] = this.finishing;
    data['quantity'] = this.quantity;
    data['price'] = this.price;
    return data;
  }
}
