class Printable {
  int id;
  String name;
  // int type;
  double maxQuantity;
  double price;

  Printable({this.id, this.name, this.maxQuantity, this.price});

  factory Printable.fromJson(Map<String, dynamic> json) {
    return Printable(
        id: json['id'],
        name: json['name'],
        // type: json['type'],
        maxQuantity: json['max_quantity'],
        price: json['price']);
  }
}
