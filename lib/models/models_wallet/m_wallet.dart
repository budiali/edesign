class Wallet {
  int id;
  int user;
  double amount;
  double hold;
  List<Transactions> transactions;

  Wallet({this.id, this.user, this.amount, this.hold, this.transactions});

  Wallet.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    user = json['user'];
    amount = json['amount'].toDouble();
    hold = json['hold'].toDouble();
    if (json['transactions'] != null) {
      transactions = new List<Transactions>();
      json['transactions'].forEach((v) {
        transactions.add(new Transactions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user'] = this.user;
    data['amount'] = this.amount;
    data['hold'] = this.hold;
    if (this.transactions != null) {
      data['transactions'] = this.transactions.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Transactions {
  int id;
  int status;
  String created;
  String modified;
  int customer;
  int partner;
  int order;

  Transactions(
      {this.id,
      this.status,
      this.created,
      this.modified,
      this.customer,
      this.partner,
      this.order});

  Transactions.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    status = json['status'];
    created = json['created'];
    modified = json['modified'];
    customer = json['customer'];
    partner = json['partner'];
    order = json['order'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['status'] = this.status;
    data['created'] = this.created;
    data['modified'] = this.modified;
    data['customer'] = this.customer;
    data['partner'] = this.partner;
    data['order'] = this.order;
    return data;
  }
}
