class CreateServiceCategory {
  int parent;
  String name;
  String desc;

  CreateServiceCategory({this.parent, this.name, this.desc});

  factory CreateServiceCategory.fromJson(Map<String, dynamic> json) {
    return CreateServiceCategory(
      parent: json['parent'] as int,
      name: json['name'] as String,
      desc: json['desc'] as String,
    );
  }
}
