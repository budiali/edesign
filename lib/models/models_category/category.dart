class GetServiceCategories {
  int id;
  int parent;
  String name;
  String image;
  String desc;

  GetServiceCategories(
      {this.id, this.parent, this.name, this.image, this.desc});

  factory GetServiceCategories.fromJson(Map<String, dynamic> map) {
    return GetServiceCategories(
      id: map['id'] as int,
      parent: map['parent'] as int,
      name: map['name'] as String,
      image: map['image'] as String,
      desc: map['desc'] as String,
    );
  }
}
