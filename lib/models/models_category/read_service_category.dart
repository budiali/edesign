class ReadServiceCategory {
  int id;
  int parent;
  String name;
  String image;
  String desc;

  ReadServiceCategory({this.id, this.parent, this.name, this.image, this.desc});

  factory ReadServiceCategory.fromJson(Map<String, dynamic> json) {
    return ReadServiceCategory(
      id: json["id"] as int,
      parent: json["parent"] as int,
      name: json["name"] as String,
      image: json["image"] as String,
      desc: json["desc"] as String,
    );
  }
}
