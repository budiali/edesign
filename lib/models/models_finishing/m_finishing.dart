class Finishing {
  int id;
  String name;
  double price;

  Finishing({this.id, this.name, this.price});

  factory Finishing.fromJson(Map<String, dynamic> json) {
    return Finishing(id: json['id'], name: json['name'], price: json['price']);
  }
}
