class ReadService {
  String id;
  List<dynamic> categories;
  String name;
  String image;
  double price;
  String desc;

  ReadService(
      {this.id, this.categories, this.name, this.image, this.price, this.desc});

  factory ReadService.convertReadService(Map<String, dynamic> json) {
    return ReadService(
      id: json["id"].toString(),
      categories: json["categories"] as List,
      name: json["name"] as String,
      image: json["image"] as String,
      price: json["price"] as double,
      desc: json["desc"] as String,
    );
  }
}
