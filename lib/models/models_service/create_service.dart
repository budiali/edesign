class Service {
  List<int> category;
  String name;
  double price;
  String desc;

  Service({this.category, this.name, this.price, this.desc});

  factory Service.fromJson(Map<String, dynamic> json) {
    return Service(
      category: json["categories"].cast<int>(),
      name: json["name"] as String,
      price: json["price"] as double,
      desc: json["desc"] as String,
    );
  }
}
