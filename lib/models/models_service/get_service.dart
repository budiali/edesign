class GetService {
  int id;
  List<dynamic> categories;
  String name;
  String image;
  double price;
  String desc;

  GetService(
      {this.id, this.categories, this.name, this.image, this.price, this.desc});

  factory GetService.fromJson(Map<String, dynamic> map) {
    return GetService(
      id: map["id"] as int,
      categories: map["categories"] as List,
      name: map["name"] as String,
      image: map["image"] as String,
      price: map["price"] as double,
      desc: map["desc"] as String,
    );
  }
}
