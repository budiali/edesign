import 'package:intl/intl.dart';

String formatNumber(num number) {
  var formatter = NumberFormat.currency(symbol: "Rp. ", locale: 'id_ID');
  return formatter.format(number);
}
