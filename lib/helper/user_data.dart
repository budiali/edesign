import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decode/jwt_decode.dart';

final storage = new FlutterSecureStorage();

Future<int> getUserId() async {
  String token = await storage.read(key: 'access');
  Map<String, dynamic> payload = Jwt.parseJwt(token);
  print(payload["user_id"]);
  return payload['user_id'];
}
