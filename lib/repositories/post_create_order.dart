import 'dart:convert';
import 'package:edesign/helper/user_data.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:edesign/models/models_order/create_order.dart';
import 'package:edesign/main.dart';

final storage = new FlutterSecureStorage();

class PostCreateOrder {
  static Future<CreateOrder> postCreateOrder() async {
    int customer = await getUserId();
    print(customer);
    String token = await storage.read(key: 'access');
    String _url = '$SERVER_IP/order/';
    final headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    };

    final body = {
      "customer": customer,
      "partner": null,
      "status": 1,
      "payment_type": 1,
      "order_items": [
        {"quantity": 3, "service": 1, "printable": 1, "finishing": 1}
      ]
    };

    final response = await http.post(Uri.parse(_url),
        headers: headers, body: json.encode(body));
    print(response.body);

    if (response.statusCode == 201) {
      print(json.decode(response.body));
      var jsonObject = json.decode(response.body);
      return CreateOrder.fromJson(jsonObject);
    } else {
      throw Exception("Failed to Create New Service");
    }
  }
}
