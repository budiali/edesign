import 'dart:convert';
import 'package:edesign/main.dart';
import 'package:edesign/models/models_signup/signup.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

final storage = new FlutterSecureStorage();

class SignUpRepository {
  static Future<SignUp> attemptSignUp(
      String username,
      String password,
      String password2,
      String email,
      String first_name,
      String last_name) async {
    final String _url = "$SERVER_IP/auth/register/";
    final headers = {"Content-Type": "application/json"};
    final body = jsonEncode({
      "username": username,
      "password": password,
      "password2": password2,
      "email": email,
      "first_name": first_name,
      "last_name": last_name
    });
    final res = await http.post(Uri.parse(_url), headers: headers, body: body);
    if (res.statusCode == 201) {
      print(json.decode(res.body));
      var jsonObject = json.decode(res.body);
      return SignUp.fromJson(jsonObject);
    } else {
      throw Exception("Failed to Create New User");
    }
    // print(res.body);
    // return null;
  }
}
