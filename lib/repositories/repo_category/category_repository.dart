import 'dart:convert';
import 'package:edesign/main.dart';
import 'package:edesign/models/models_category/category.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

class CategoryRepository {
  //GetServiceCategories class is in the folder models.
  static Future<List<GetServiceCategories>> getCategories() async {
    final _storage = new FlutterSecureStorage();
    String token = await _storage.read(key: 'access');
    final response = await http.get(Uri.parse('$SERVER_IP/service/category/'),
        headers: {"Authorization": 'Bearer ' + token});

    if (response.statusCode == 200) {
      List<GetServiceCategories> listCategories =
          parseGetServiceCategories(response.body);
      return listCategories;
    } else {
      throw Exception('Failed to load Category Design from API');
    }
    // print(response.body);
  }

  static List<GetServiceCategories> parseGetServiceCategories(
      String responseBody) {
    final Map<String, dynamic> parsed = json.decode(responseBody);
    return List<GetServiceCategories>.from(
        parsed["results"].map((x) => GetServiceCategories.fromJson(x)));
  }
}
