import 'dart:convert';
import 'package:edesign/main.dart';
import 'package:edesign/models/models_category/read_service_category.dart';
import 'package:http/http.dart' as http;

class ReadServiceCategoryRepository {
  static Future<ReadServiceCategory> fetchServiceCategoryRead(
      String token) async {
    try {
      final response = await http.get(
          Uri.parse('$SERVER_IP/service/category/6/'),
          headers: {"Authorization": "Bearer " + token});
      if (response.statusCode == 200) {
        return ReadServiceCategory.fromJson(json.decode(response.body));
      } else {
        throw Exception('Failed to load Data from API');
      }
    } catch (e) {
      // print(e);
      throw e;
    }
  }
}
