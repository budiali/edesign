import 'dart:convert';
import 'package:edesign/main.dart';
import 'package:edesign/models/models_category/m_create_service_category.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

class CreateServiceCategoryRepository {
  static Future<CreateServiceCategory> createServiceCategory(
      int parent, String name, String desc) async {
    final _storage = new FlutterSecureStorage();
    String token = await _storage.read(key: "access");
    final String _urlApi = "$SERVER_IP/service/category/";

    final headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    };

    final body = {"parent": parent, "name": name, "desc": desc};

    final response = await http.post(Uri.parse(_urlApi),
        headers: headers, body: json.encode(body));

    if (response.statusCode == 201) {
      // print(json.decode(response.body));
      var jsonObject = json.decode(response.body);
      return CreateServiceCategory.fromJson(jsonObject);
    } else {
      throw "Failed to POST data service category!";
    }
  }
}
