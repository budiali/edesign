import 'dart:convert';
// import 'dart:js';
import 'package:edesign/helper/user_data.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:edesign/main.dart';

final storage = new FlutterSecureStorage();

class PostCreateOrder {
  static Future<Map<String, dynamic>> postCreateOrder(
      int id, int quantity, int _mySelection, int _mySelected) async {
    int customer = await getUserId();
    String token = await storage.read(key: 'access');
    String _url = '$SERVER_IP/order/';
    final headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    };

    final body = {
      "customer": customer,
      // "partner": null,
      "status": 1,
      "payment_type": 1,
      "order_items": [
        {
          "service": id,
          "quantity": quantity != null ? quantity : 1,
          "printable": _mySelection,
          "finishing": _mySelected
        }
      ]
    };

    final response = await http.post(Uri.parse(_url),
        headers: headers, body: json.encode(body));
    // print(response.body);
    if (response.statusCode == 400) {
      return jsonDecode(response.body);
    } else if (response.statusCode == 201) {
      var jsonObject = json.decode(response.body);
      // print(jsonObject);
      return jsonObject;
    } else {
      throw Exception("Failed to Create New Order");
    }
  }
}
