import 'dart:convert';
import 'package:edesign/main.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:edesign/models/models_order/order.dart';

final storage = FlutterSecureStorage();

class ReadOrderItemRepository {
  // ignore: missing_return
  static Future<Order> fetchOrderItemId(String id) async {
    final String token = await storage.read(key: 'access');

    String _url = '$SERVER_IP/order/$id/';
    final headers = {'Authorization': 'Bearer ' + token};

    var response = await http.get(Uri.parse(_url), headers: headers);

    try {
      if (response.statusCode == 200) {
        // print(json.decode(response.body));
        var jsonObj = json.decode(response.body);
        // path in models/models_order/create_order.dart
        // print("Jumlah data => $stores");
        return Order.fromJson(jsonObj);
      }
    } catch (e) {
      throw e;
    }
  }
}
