import 'dart:convert';
import 'package:edesign/main.dart';
import 'package:edesign/models/models_order/order.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

final storage = new FlutterSecureStorage();

class GetOrderItemRepository {
  static Future<Order> getOrderItem(String id) async {
    String token = await storage.read(key: "access");
    try {
      final response = await http.get(Uri.parse("$SERVER_IP/order/$id/"),
          headers: {"Authorization": "Bearer " + token});
      if (response.statusCode == 200) {
        // print(jsonDecode(response.body));
        // List<OrderItem> dataOrderItem = parseOrderItem(response.body);
        var jsonObj = json.decode(response.body);
        // print(jsonObj);
        var order = Order.fromJson(jsonObj);
        return order;
      } else {
        throw "Failed to load data from API";
      }
    } catch (e) {
      throw e;
    }
  }

  // static List<OrderItem> parseOrderItem(String responseBody) {
  //   final Map<String, dynamic> parsed = json.decode(responseBody);
  //   return List<OrderItem>.from(
  //       parsed["results"].map((x) => OrderItem.fromJson(x)));
  // }

  static Future<List<Order>> getOrder() async {
    String token = await storage.read(key: 'access');
    String _urlOrder = '$SERVER_IP/order/';
    try {
      var response = await http.get(Uri.parse(_urlOrder),
          headers: {'Authorization': 'Bearer ' + token});
      if (response.statusCode == 200) {
        List<Order> jsonObj = parsedJsonObj(response.body);
        return jsonObj;
      } else {
        throw Exception('Failed to load data Order from API');
      }
    } catch (e) {
      throw e;
    }
  }

  static List<Order> parsedJsonObj(String responseBody) {
    final Map<String, dynamic> parsed = json.decode(responseBody);
    return List<Order>.from(parsed['results'].map((x) => Order.fromJson(x)));
  }
}
