import 'dart:convert';
import 'package:edesign/main.dart';
import 'package:edesign/models/models_wallet/m_wallet.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

final storage = new FlutterSecureStorage();

class WalletRepository {
  // ignore: missing_return
  static Future<Wallet> getWallet() async {
    String token = await storage.read(key: "access");
    final response = await http.get(Uri.parse("$SERVER_IP/wallet/"),
        headers: {"Authorization": "Bearer " + token});
    if (response.statusCode == 200) {
      // print(response.body);
      var jsonObj = json.decode(response.body);
      var data = Wallet.fromJson(jsonObj);
      return data;
    } else {
      print("object not found");
    }
  }
}
