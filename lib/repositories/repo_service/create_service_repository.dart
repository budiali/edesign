import 'dart:convert';
import 'package:edesign/main.dart';
import 'package:edesign/models/models_service/create_service.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

final storage = new FlutterSecureStorage();

class CreateServiceRepository {
  static Future<Service> createService(
      int categories, String name, double price, String desc) async {
    String token = await storage.read(key: 'access');
    final String urlApi = "$SERVER_IP/service/";
    final headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    };
    final body = {
      "categories": [categories],
      "name": name,
      "price": price,
      "desc": desc
    };

    final response = await http.post(Uri.parse(urlApi),
        headers: headers, body: json.encode(body));

    if (response.statusCode == 201) {
      // print(json.decode(response.body));
      var jsonObject = json.decode(response.body);
      return Service.fromJson(jsonObject);
    } else {
      throw Exception("Failed to Create New Service");
    }
  }

  static Future<Service> updateService() async {
    String token = await storage.read(key: 'access');
    final String urlApi = "$SERVER_IP/service/{id}/";
    final headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token
    };
    final body = {
      "categories"
          "name"
          "price"
          "desc"
    };

    final response = await http.put(Uri.parse(urlApi),
        headers: headers, body: json.encode(body));

    if (response.statusCode == 200) {
      var jsonObject = json.decode(response.body);
      return Service.fromJson(jsonObject);
    } else {
      throw Exception("Failed to Update Service!");
    }
  }
}
