import 'dart:convert';
import 'package:edesign/main.dart';
import 'package:edesign/models/models_service/read_service.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

class ReadServiceRepository {
  //ReadService class is in the folder models.
  // ignore: missing_return
  static Future<ReadService> fetchReadService(String id) async {
    final _storage = new FlutterSecureStorage();
    String token = await _storage.read(key: "access");
    try {
      final response = await http.get(Uri.parse("$SERVER_IP/service/" + id),
          headers: {"Authorization": "Bearer " + token});

      if (response.statusCode == 200) {
        // print(json.decode(response.body));
        return ReadService.convertReadService(json.decode(response.body));
      }
    } catch (e) {
      // print(e);
      throw e;
    }
  }
}
