import 'dart:convert';
import 'package:edesign/main.dart';
import 'package:edesign/models/models_service/get_service.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

final storage = new FlutterSecureStorage();

class GetServiceRepository {
  static Future<List<GetService>> getService(String categoryId) async {
    final String token = await storage.read(key: 'access');
    try {
      final response = await http.get(
          Uri.parse("$SERVER_IP/service/?category=$categoryId"),
          headers: {"Authorization": 'Bearer ' + token});
      if (response.statusCode == 200) {
        // print(json.decode(response.body));
        List<GetService> listGetService = parseGetService(response.body);
        return listGetService;
      } else {
        throw Exception('Failed to load Category Design from API');
      }
    } catch (e) {
      // print(e);
      throw e;
    }
  }

  static List<GetService> parseGetService(String responseBody) {
    final Map<String, dynamic> parsed = json.decode(responseBody);
    return List<GetService>.from(
        parsed["results"].map((x) => GetService.fromJson(x)));
  }
}
