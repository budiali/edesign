import 'dart:convert';
import 'package:edesign/models/models_printable/m_printable.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import '../../main.dart';

final storage = new FlutterSecureStorage();

class PrintableRepository {
  static Future<List<Printable>> getPrintables() async {
    final String url = "$SERVER_IP/printables/";
    final String token = await storage.read(key: 'access');
    final response = await http.get(
      Uri.parse(url),
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + token
      },
    );

    if (response.statusCode == 200) {
      List<Printable> jsonObj = parsedJson(response.body);
      return jsonObj;
    } else {
      throw Exception('Failed to load data Order from API');
    }
  }

  static List<Printable> parsedJson(String responseBody) {
    final Map<String, dynamic> parsed = json.decode(responseBody);
    return List<Printable>.from(
        parsed['results'].map((x) => Printable.fromJson(x)));
    // final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    // return parsed.map<Printable>((json) => Printable.fromJson(json)).toList();
  }
}
