import 'package:edesign/models/models_finishing/m_finishing.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import '../../main.dart';

final storage = new FlutterSecureStorage();

class FinishingRepository {
  static Future<List<Finishing>> getFinishing() async {
    final String token = await storage.read(key: 'access');
    final String _url = "$SERVER_IP/printables/finishing/";
    final response = await http.get(Uri.parse(_url), headers: {
      "Content-Type": "application/json",
      'Authorization': 'Bearer ' + token
    });

    if (response.statusCode == 200) {
      List<Finishing> data = parsedJsonObj(response.body);
      return data;
    } else {
      throw Exception('Failed load data form API');
    }
  }

  static List<Finishing> parsedJsonObj(String responseBody) {
    final Map<String, dynamic> parsed = json.decode(responseBody);
    return List<Finishing>.from(
        parsed['results'].map((x) => Finishing.fromJson(x)));
  }
}
